## Build the binary ##########################################################
FROM rust:1.42 as build

WORKDIR /claimchain

COPY Cargo.toml Cargo.lock ./

# Create a dummy `main.rs` to allow `cargo fetch`
RUN mkdir src && touch src/main.rs

# Get dependencies
RUN cargo fetch

COPY src src

RUN cargo build --release


## Build the final image #####################################################
FROM debian:stable-slim

RUN apt-get -y update --fix-missing && \
    apt-get upgrade -y && \
    apt-get -y install --fix-missing openssl ca-certificates && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY --from=build /claimchain/target/release/claimchain /usr/bin/claimchain

CMD claimchain