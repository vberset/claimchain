# claimchain

> A toy blockchain

## Usage

```shell script
$ claimchain [--interface <interface>] [<peer> ...]
```

## License

* [MIT License](https://choosealicense.com/licenses/mit/)
* Copyright 2020 © Vincent Berset