use std::fmt;

use argon2;
use bytes::{BufMut, BytesMut};
use chrono::{DateTime, Utc};

use crate::transaction::Transaction;
use crate::utils::{format_hash, format_hexdump};

/// Convert block header's info to raw bytes, but nonce.
fn to_raw_header(
    index: u64,
    parent_hash: &[u8],
    timestamp: DateTime<Utc>,
    merkle_root: &[u8],
) -> Vec<u8> {
    let mut raw_data = BytesMut::with_capacity(1024);
    raw_data.put_u64_le(index);
    raw_data.put(parent_hash);
    raw_data.put_i64_le(timestamp.timestamp());
    raw_data.put(merkle_root);
    raw_data.to_vec()
}

/// Convert nonce to raw bytes.
fn to_raw_nonce(nonce: u64) -> Vec<u8> {
    let mut raw_nonce = BytesMut::with_capacity(128);
    raw_nonce.put_u64_le(nonce);
    raw_nonce.put_u64_le(0);
    raw_nonce.to_vec()
}

/// Hash
pub(crate) fn hash_proto_block(
    index: u64,
    parent_hash: &[u8],
    timestamp: DateTime<Utc>,
    nonce: u64,

    merkle_root: &[u8],
) -> Vec<u8> {
    let config = argon2::Config {
        variant: argon2::Variant::Argon2id,
        version: argon2::Version::Version13,
        mem_cost: 65536,
        time_cost: 10,
        lanes: 4,
        thread_mode: argon2::ThreadMode::Parallel,
        secret: &[],
        ad: &[],
        hash_length: 64,
    };
    let raw_block_header = to_raw_header(index, parent_hash, timestamp, merkle_root);
    let raw_nonce = to_raw_nonce(nonce);
    argon2::hash_raw(raw_block_header.as_ref(), raw_nonce.as_ref(), &config).unwrap()
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Block {
    pub hash: Vec<u8>,
    pub index: u64,
    pub parent_hash: Vec<u8>,
    pub timestamp: DateTime<Utc>,
    pub nonce: u64,
    pub merkle_root: Vec<u8>,
    pub transactions: Vec<Transaction>,
}

impl Block {
    pub fn rehash(&self) -> Vec<u8> {
        hash_proto_block(
            self.index,
            &self.parent_hash,
            self.timestamp,
            self.nonce,
            &self.merkle_root,
        )
    }

    pub fn is_valid(&self) -> bool {
        self.hash == self.rehash()
    }

    pub fn is_valid_genesis(&self) -> bool {
        self.parent_hash == vec![0; 64] && self.index == 0 && self.is_valid()
    }

    pub fn is_valid_child_of(&self, parent: &Block) -> bool {
        parent.index + 1 == self.index && parent.hash == self.parent_hash && self.is_valid()
    }
}

impl fmt::Display for Block {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Block #{}\n\n", self.index)?;
        write!(
            f,
            " Parent Hash:\n{}\n",
            format_hash(&self.parent_hash, 16, 4)
        )?;
        write!(f, " Block Hash:\n{}\n", format_hash(&self.hash, 16, 4))?;
        write!(f, " Timestamp:\n    {}\n\n", self.timestamp)?;
        write!(f, " Nonce:\n    {}\n\n", self.nonce)?;
        write!(
            f,
            " Merkle Root:\n{}\n",
            format_hexdump(&self.merkle_root, 16, 4)
        )
    }
}
