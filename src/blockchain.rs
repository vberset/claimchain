//! Blockchain's core data structures and algorithms.
//!
//! The `Blockchain` struct stores a valid chain of blocks, represented
//! by the `Block` struct. The `Blockchain` public API guarantees the blockchain
//! integrity.
use std::collections::HashMap;
use std::slice;
use std::u64;

use chrono::Utc;
use log::debug;

use crate::block::{hash_proto_block, Block};
use crate::transaction::{compute_merkle_root, Transaction};

#[derive(Debug)]
pub enum Error {
    Integrity,
    Mining,
}

#[derive(Debug, Clone)]
pub struct Blockchain {
    difficulty: u8,
    chain: Vec<Block>,
    block_map: HashMap<Vec<u8>, u64>,
}

fn is_valid_subchain(subchain: &[Block]) -> bool {
    if let Some(head) = subchain.first() {
        if !head.is_valid() {
            return false;
        }
    }

    for couple in subchain.windows(2) {
        if !&couple[1].is_valid_child_of(&couple[0]) {
            return false;
        }
    }

    true
}

impl Blockchain {
    pub fn new(difficulty: u8) -> Self {
        Self {
            difficulty,
            chain: vec![],
            block_map: HashMap::new(),
        }
    }

    pub fn from_blocks(difficulty: u8, blocks: Vec<Block>) -> Result<Self, Error> {
        if !is_valid_subchain(&blocks) {
            return Err(Error::Integrity);
        }

        let mut blockchain = Self::new(difficulty);
        blockchain.chain = blocks;
        Ok(blockchain)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn len(&self) -> usize {
        self.chain.len()
    }

    #[inline]
    #[allow(dead_code)]
    pub fn contains(&self, block: &Block) -> bool {
        self.chain
            .get(block.index as usize)
            .map_or(false, |member| member == block)
    }

    /// Check the validity and the integrity of all the blocks of the chain.
    #[allow(dead_code)]
    pub fn is_valid(&self) -> bool {
        is_valid_subchain(&self.chain) && self.chain[0].is_valid_genesis()
    }

    /// Mines a new block to store the given data.
    ///
    /// The block is *not* append to the blockchain.
    pub fn mine(&self, transactions: &[Transaction]) -> Result<Block, Error> {
        debug!("Mine new block");
        let difficulty_pattern = "0".repeat(self.difficulty as usize);

        let timestamp = Utc::now();
        let index = self.chain.len();
        let parent_hash = match self.chain.last() {
            Some(parent_block) => parent_block.hash.clone(),
            None => vec![0; 64],
        };
        let merkle_root = compute_merkle_root(transactions);

        for nonce in 0..=u64::MAX {
            let hash = hash_proto_block(index as u64, &parent_hash, timestamp, nonce, &merkle_root);
            let hex_hash = hex::encode(&hash);

            if hex_hash.starts_with(&difficulty_pattern) {
                let block = Block {
                    hash,
                    index: index as u64,
                    parent_hash,
                    timestamp,
                    nonce,
                    merkle_root,
                    transactions: transactions.to_owned(),
                };

                debug!("Successfuly mine block {}", hex::encode(&block.hash));
                return Ok(block);
            }
        }

        Err(Error::Mining)
    }

    /// Extends the blockchain with given blocks. If the extend succeed,
    /// the evicted blocks are returned. If it fails, the specific error is returned.
    /// The `extends` methods can't change the root block.
    pub fn extends(&mut self, blocks: Vec<Block>) -> Result<Vec<Block>, Error> {
        let first_new_block = blocks.first().ok_or(Error::Integrity)?;
        let pivot = first_new_block.index as usize;

        if pivot != 0 {
            match self.chain.get(pivot - 1) {
                Some(parent) => {
                    if !first_new_block.is_valid_child_of(parent) {
                        return Err(Error::Integrity);
                    }
                }
                None => return Err(Error::Integrity),
            };
        }

        // check subchain
        if !is_valid_subchain(&blocks) {
            return Err(Error::Integrity);
        }

        // substitute blocks
        let evicted_blocks = self.chain.split_off(pivot);

        for block in blocks.iter() {
            self.block_map.insert(block.hash.clone(), block.index);
        }
        self.chain.extend_from_slice(&blocks);

        for block in evicted_blocks.iter() {
            self.block_map.remove(&block.hash);
        }

        Ok(evicted_blocks)
    }

    pub fn get_block_by_hash(&self, hash: &[u8]) -> Option<&Block> {
        let index = *self.block_map.get(hash)?;
        self.chain.get(index as usize)
    }

    #[inline]
    pub fn iter(&self) -> slice::Iter<'_, Block> {
        self.chain.iter()
    }
}
