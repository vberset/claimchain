use std::collections::{BTreeSet, HashMap, btree_set};
use std::net::SocketAddr;
use std::slice;

use chrono::{DateTime, Utc};
use sha3::{Digest, Sha3_256};
use std::cmp::Ordering;

enum Tree {
    Branch {
        value: Vec<u8>,
        left: Option<Box<Tree>>,
        right: Option<Box<Tree>>,
    },
    Leaf(KBucket)
}

#[derive(Clone, PartialEq, Eq, Hash)]
struct Node {
    addr: SocketAddr,
    id: Vec<u8>,
    last_activity: DateTime<Utc>,
}

impl Node {
    fn from_addr(addr: SocketAddr) -> Self {
        let mut hasher = Sha3_256::default();
        hasher.input(addr.to_string().as_bytes());
        Self {
            addr,
            id: hasher.result().to_vec(),
            last_activity: Utc::now(),
        }
    }

    fn touch(&mut self) {
        self.last_activity = Utc::now();
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        for (left, right) in self.id.iter().zip(&other.id) {
            if left != right {
                return left.partial_cmp(right);
            }
        }

        self.id.len().partial_cmp(&other.id.len())
    }
}

impl Ord for Node {
    #[inline]
    fn cmp(&self, other: &Node) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

struct KBucket {
    k: u8,
    nodes: BTreeSet<Node>,
}

impl KBucket {
    fn new(k: u8) -> Self {
        Self {
            k,
            nodes: BTreeSet::new(),
        }
    }

    fn insert(&mut self, node: Node) -> bool {
        if self.nodes.contains(&node) {
            true
        } else if self.nodes.len() > self.k as usize {
            false
        } else {
            self.nodes.insert(node);
            true
        }
    }

    fn in_range(&self, node: &Node) -> bool {
        let first = self.nodes.iter().next();
        let last = self.nodes.iter().last();
        if let (Some(first), Some(last)) = (first, last) {
            first <= node && node <= last
        } else {
            false
        }
    }

    fn split(mut self, pivot_node: Node) -> (Self, Self) {
        let right = Self {
            k: self.k,
            nodes: self.nodes.split_off(&pivot_node),
        };
        (self, right)
    }

    #[inline]
    fn iter(&self) -> btree_set::Iter<'_, Node> {
        self.nodes.iter()
    }
}

pub struct DistributedRoutingTable {}

impl DistributedRoutingTable {
    pub fn new(id: Vec<u8>, k: u8) -> Self {
        Self {}
    }

    pub fn insert_peer(&mut self, peer: SocketAddr) -> bool {
        false
    }

    pub fn iter(&self) -> Iter {
        Iter {}
    }
}

pub struct Iter {}
