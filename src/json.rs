use chrono::{DateTime, Utc};
use ed25519_dalek::{PublicKey, Signature};
use serde::{Deserialize, Serialize};

use crate::block::Block;
use crate::transaction::Transaction;

#[derive(Serialize, Deserialize)]
pub struct JsonBlock {
    pub index: u64,
    pub parent_hash: String,
    pub timestamp: DateTime<Utc>,
    pub nonce: u64,
    pub merkle_root: String,
    pub transactions: Vec<JsonTransaction>,
}

impl From<&Block> for JsonBlock {
    fn from(block: &Block) -> Self {
        Self {
            index: block.index,
            parent_hash: hex::encode(&block.parent_hash),
            timestamp: block.timestamp,
            nonce: block.nonce,
            merkle_root: hex::encode(&block.merkle_root),
            transactions: block.transactions.iter().map(|t| t.into()).collect(),
        }
    }
}

impl Into<Block> for JsonBlock {
    fn into(self) -> Block {
        let mut block = Block {
            index: self.index,
            parent_hash: hex::decode(&self.parent_hash).unwrap(),
            timestamp: self.timestamp,
            nonce: self.nonce,
            merkle_root: hex::decode(&self.merkle_root).unwrap(),
            transactions: self.transactions.into_iter().map(|t| t.into()).collect(),
            hash: vec![],
        };
        block.hash = block.rehash();
        block
    }
}

#[derive(Serialize, Deserialize)]
pub struct JsonTransaction {
    pub digest: String,
    pub pub_key: String,
    pub signature: String,
}

impl From<&Transaction> for JsonTransaction {
    fn from(transaction: &Transaction) -> Self {
        Self {
            digest: hex::encode(&transaction.digest),
            pub_key: hex::encode(&transaction.pub_key),
            signature: hex::encode(transaction.signature.to_bytes().as_ref()),
        }
    }
}

impl Into<Transaction> for JsonTransaction {
    fn into(self) -> Transaction {
        Transaction {
            digest: hex::decode(&self.digest).unwrap(),
            pub_key: PublicKey::from_bytes(hex::decode(&self.pub_key).unwrap().as_slice()).unwrap(),
            signature: Signature::from_bytes(hex::decode(&self.signature).unwrap().as_slice())
                .unwrap(),
        }
    }
}
