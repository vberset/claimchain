mod block;
mod blockchain;
mod json;
mod node;
mod transaction;
mod utils;
mod web;

use std::collections::HashSet;
use std::env;

use actix_rt;
use clap::{App, Arg};

use crate::node::Node;

/*
*/

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let matches = App::new("claimchain")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Vincent Berset <vberset@protonmail.ch>")
        .about("claimchain node")
        .arg(Arg::with_name("interface")
            .short("i")
            .long("interface")
            .value_name("INTERFACE")
            .help("Bind to the specified network interface")
            .default_value("0.0.0.0")
            .takes_value(true))
        .arg(Arg::with_name("peers")
            .multiple(true)
            .help("Bootstrap nodes"))
        .get_matches();

    let interface = matches.value_of("interface").unwrap();
    let peers = if let Some(peers) = matches.values_of("peers") {
        peers.map(|p| p.parse().unwrap()).collect()
    } else {
        HashSet::new()
    };

    let node = Node::new();
    node.join_network(&interface, peers).await
}
