use std::collections::{HashSet, VecDeque, hash_set};
use std::net::IpAddr;
use std::slice;

use actix_web::client::Client;
use log::debug;
use parking_lot::RwLock;

use crate::block::Block;
use crate::blockchain::{Blockchain, Error};
use crate::json::JsonBlock;
use crate::web;
use crate::transaction::Transaction;

pub const PORT: u16 = 8787;

/// Clamchain's node state
pub struct Node {
    blockchain: Blockchain,
    peers: HashSet<IpAddr>,
}

impl Node {
    /// Create a new fresh node with no blocks
    pub fn new() -> Self {
        Self {
            blockchain: Blockchain::new(3),
            peers: HashSet::new(),
        }
    }

    /// Create a new node initialized with the given blocks
    #[allow(dead_code)]
    pub fn from_blocks(blocks: Vec<Block>) -> Result<Self, Error> {
        Ok(Self {
            blockchain: Blockchain::from_blocks(3, blocks)?,
            peers: HashSet::new(),
        })
    }

    /// Join the network of the given peers
    pub async fn join_network(mut self, interface: &str, peers: HashSet<IpAddr>) -> std::io::Result<()> {
        debug!("Bootstrap network");
        let mut queue: VecDeque<_> = peers.into_iter().collect();
        while let Some(peer) = queue.pop_front() {
            if self.peers.insert(peer.clone()) {
                for peer in fetch_peers(&peer).await {
                    if self.add_peer(peer.clone()) {
                        queue.push_back(peer)
                    }
                }
                self.add_peer(peer);
            }
        }

        self_announce(&self.peers).await;

        let node = web::Data::new(RwLock::new(self));
        debug!("Start web server");
        web::run_web_api(&interface, PORT, node).await
    }

    #[inline]
    pub fn submit_block(&mut self, block: Block) {
        debug!("Block submitted: {}", hex::encode(&block.hash));
        let blocks = vec![block];
        self.blockchain.extends(blocks).unwrap();
    }

    #[inline]
    pub fn get_block_by_hash(&self, hash: &[u8]) -> Option<&Block> {
        self.blockchain.get_block_by_hash(&hash)
    }

    #[inline]
    pub fn iter_blocks(&self) -> slice::Iter<'_, Block> {
        self.blockchain.iter()
    }

    #[inline]
    pub fn iter_peers(&self) -> hash_set::Iter<'_, IpAddr> {
        self.peers.iter()
    }

    #[inline]
    pub fn add_peer(&mut self, peer_addr: IpAddr) -> bool {
        debug!("Add peer {}", &peer_addr);
        self.peers.insert(peer_addr.into())
    }

    #[inline]
    pub fn mine_block(&self, tx: &[Transaction]) -> Result<Block, Error> {
        self.blockchain.mine(tx)
    }
}

/// Fetch the peers list from another node
async fn fetch_peers(peer: &IpAddr) -> HashSet<IpAddr> {
    let client = Client::default();
    client
        .get(format!("http://{}:{}/peers", peer, PORT))
        .send()
        .await
        .unwrap()
        .json::<Vec<String>>()
        .await
        .unwrap()
        .iter()
        .map(|p| p.parse().unwrap())
        .collect()
}

///
async fn self_announce(peers: &HashSet<IpAddr>) {
    let client = Client::default();

    for peer in peers {
        debug!("Self announce to {}", &peer);
        let url = format!("http://{}:{}/peers", peer, PORT);
        client.post(url).send_body("HELLO").await.unwrap();
    }
}

#[allow(dead_code)]
async fn fetch_blockchain(peer: &IpAddr) -> Vec<Block> {
    let client = Client::default();
    let hashes = client
        .get(format!("http://{}:{}/blocks", peer, PORT))
        .send()
        .await
        .unwrap()
        .json::<Vec<String>>()
        .await
        .unwrap();

    let mut blocks = vec![];
    for hash in hashes {
        let url = format!("http://{}:{}/blocks/{}", peer, PORT, hash);
        let block = client
            .get(url)
            .send()
            .await
            .unwrap()
            .json::<JsonBlock>()
            .await
            .unwrap()
            .into();
        blocks.push(block);
    }
    blocks
}
