//! Transaction structure and utility functions.
//!
//! A transaction made of a file digest, a public key, and the digest's
//! signature.
use ed25519_dalek::{PublicKey, Signature};
use sha3::{Digest, Sha3_256};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Transaction {
    pub digest: Vec<u8>,
    pub pub_key: PublicKey,
    pub signature: Signature,
}

impl Transaction {
    pub fn is_valid(&self) -> bool {
        self.pub_key.verify(&self.digest, &self.signature).is_ok()
    }

    pub fn hash(&self) -> Vec<u8> {
        let mut hasher = Sha3_256::default();
        hasher.input(&self.digest);
        hasher.input(&self.pub_key.as_bytes());
        hasher.input(&self.signature.to_bytes()[..]);
        hasher.result().to_vec()
    }
}

fn merge_hashes(h1: &[u8], h2: &[u8]) -> Vec<u8> {
    let mut hasher = Sha3_256::default();
    hasher.input(h1);
    hasher.input(h2);
    hasher.result().to_vec()
}

pub fn compute_merkle_root(transactions: &[Transaction]) -> Vec<u8> {
    assert!(!transactions.is_empty());

    let mut leafs: Vec<_> = transactions.iter().map(|tx| tx.hash()).collect();
    loop {
        if leafs.len() & 1 == 1 {
            leafs.push(leafs.last().unwrap().clone());
        }

        if leafs.len() == 2 {
            break;
        }

        leafs = leafs
            .chunks(2)
            .map(|chunk| merge_hashes(&chunk[0], &chunk[1]))
            .collect();
    }
    merge_hashes(&leafs[0], &leafs[1])
}
