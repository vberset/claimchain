use std::iter::repeat;

pub fn format_hash(hash: &[u8], width: usize, indent: usize) -> String {
    let mut output = String::new();
    let indent = repeat(" ").take(indent).collect::<String>();

    for chunk in hash.chunks(width) {
        output.push_str(&indent);
        for byte in chunk {
            output.push_str(&format!("{:0>2X}", byte));
            output.push(':');
        }
        output.pop();
        output.push('\n');
    }

    output
}

pub fn format_hexdump(data: &[u8], width: usize, indent: usize) -> String {
    let mut output = String::new();
    let indent = repeat(" ").take(indent).collect::<String>();

    for (i, chunk) in data.chunks(width).enumerate() {
        output.push_str(&indent);
        output.push_str(&format!("{:0>6X}  ", i * width));

        for word in chunk.chunks(2) {
            output.push_str(&format!("{:0>2X}", word[0]));
            if word.len() == 2 {
                output.push_str(&format!("{:0>2X}", word[1]));
            }
            output.push(' ');
        }
        output.pop();
        output.push('\n');
    }

    output
}
