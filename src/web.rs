pub use actix_web::web::Data;
use actix_web::{client::Client, web, App, HttpRequest, HttpResponse, HttpServer, Responder};
use log::debug;
use parking_lot::RwLock;

use crate::block::Block;
use crate::blockchain::Error;
use crate::json::{JsonBlock, JsonTransaction};
use crate::node::{Node, PORT};
use crate::transaction::Transaction;

type RefNode = Data<RwLock<Node>>;

async fn submit_transaction(
    node: RefNode,
    transaction: web::Json<JsonTransaction>,
) -> impl Responder {
    let transaction: Transaction = transaction.into_inner().into();
    if !transaction.is_valid() {
        return HttpResponse::BadRequest().body("Invalid signature");
    }
    let hash = hex::encode(&transaction.hash());
    debug!("Submitted transaction {}", hex::encode(&hash));

    mine_block(node, transaction).await;

    HttpResponse::Ok().body(hash)
}

async fn submit_block(node: RefNode, block: web::Json<JsonBlock>) -> impl Responder {
    let block = block.into_inner().into();
    let mut node = node.write();
    node.submit_block(block);
    HttpResponse::NoContent().finish()
}

async fn block_details(node: RefNode, path: web::Path<String>) -> impl Responder {
    match hex::decode(&*path) {
        Ok(hash) => {
            let node = node.read();
            if let Some(block) = node.get_block_by_hash(&hash) {
                HttpResponse::Ok().json(JsonBlock::from(block))
            } else {
                HttpResponse::NotFound().finish()
            }
        }
        Err(_) => HttpResponse::NotFound().finish(),
    }
}

async fn block_list(node: RefNode) -> impl Responder {
    let node = node.read();
    let blocks = node.iter_blocks()
        .map(|block| hex::encode(&block.hash))
        .collect::<Vec<_>>();
    HttpResponse::Ok().json(blocks)
}

async fn ping() -> impl Responder {
    "PONG"
}

async fn get_peers(node: RefNode) -> impl Responder {
    let node = node.read();
    HttpResponse::Ok().json(node.iter_peers().map(|p| format!("{}", p)).collect::<Vec<_>>())
}

async fn peer_announce(req: HttpRequest, node: RefNode) -> impl Responder {
    let peer_addr = req.peer_addr().unwrap().ip();
    let mut node = node.write();
    node.add_peer(peer_addr);
    HttpResponse::Ok().finish()
}

pub async fn run_web_api(interface: &str, port: u16, node: RefNode) -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .app_data(node.clone())
            .route("/ping", web::get().to(ping))
            .route("/blocks", web::get().to(block_list))
            .route("/blocks", web::post().to(submit_block))
            .route("/blocks/{hash}", web::get().to(block_details))
            .route("/transactions", web::post().to(submit_transaction))
            .route("/peers", web::get().to(get_peers))
            .route("/peers", web::post().to(peer_announce))
    })
        .bind((interface, port))?
        .run()
        .await?;

    Ok(())
}

async fn mine_block(node: RefNode, transaction: Transaction) {
    let block = {
        let transactions = vec![transaction];
        let node = node.clone();
        let block = web::block(move || -> Result<Block, Error> {
            let block = {
                let node = node.read();
                node.mine_block(&transactions)?
            };

            {
                let mut node = node.write();
                node.submit_block(block.clone());
            }

            Ok(block)
        })
            .await
            .unwrap();
        block
    };

    let jblock = JsonBlock::from(&block);
    let client = Client::default();
    debug!("Send new block {}", hex::encode(&block.hash));
    let node = node.read();
    for peer in node.iter_peers() {
        let url = format!("http://{}:{}/blocks", peer, PORT);
        client.post(url).send_json(&jblock).await.unwrap();
    }
}
